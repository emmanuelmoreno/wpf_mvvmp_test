﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NoPrismMVVMPSample
{
    public class MainWindowModel : INotifyPropertyChanged
    {
        string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Message"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

    }
}
