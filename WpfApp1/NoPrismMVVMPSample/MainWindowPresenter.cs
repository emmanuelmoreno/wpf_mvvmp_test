﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoPrismMVVMPSample
{
    class MainWindowPresenter
    {
        readonly MainWindowViewModel _viewModel;
        
        public IMainWindow View { get; private set; }

        public MainWindowPresenter(IMainWindow view)
        {
            View = view;
            _viewModel = new MainWindowViewModel
            {
                KeepAlive = this,
                ClickCommand = new DelegateCommand(ClickCommand_Execute, _ => true)
            };

            View.DataContext = _viewModel;
        }

        void ClickCommand_Execute(object args)
        {
            _viewModel.Model.Message = "Hello, World";
        }
    }
}
