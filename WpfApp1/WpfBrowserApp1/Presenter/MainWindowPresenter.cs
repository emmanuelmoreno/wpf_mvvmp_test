﻿using WpfBrowserApp1.Common;
using WpfBrowserApp1.ViewModel;

namespace WpfBrowserApp1.Presenter
{
    public class MainWindowPresenter
    {

        readonly MainWindowViewModel _viewModel;

        public IMainWindow View { get; private set; }

        public MainWindowPresenter(IMainWindow view)
        {
            View = view;
            _viewModel = new MainWindowViewModel
            {
                KeepAlive = this,
                ClickCommand = new DelegateCommand(ClickCommand_Execute, _ => true)
            };

            View.DataContext = _viewModel;
        }

        void ClickCommand_Execute(object args)
        {
            _viewModel.Model.Message = "Hello, World";
        }
    }
}
