﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfBrowserApp1.Common
{
   public interface IMainWindow
    {
        object DataContext { get; set; }
    }
}
